## 1. create secret in GKE:
```
kubectl create secret docker-registry elyusron-gitlab \
  --docker-email=mayang.yusron@gmail.com \
  --docker-username=elyusron \
  --docker-password=pass1234 \
  --docker-server=registry.gitlab.com
```


## 2 create service account as owner:
```
- gcloud iam service-accounts create {service-account-name}
- gcloud projects add-iam-policy-binding {gcp-project} --member "serviceAccount:{service-account-name}@{gcp-project}.iam.gserviceaccount.com" --role "roles/owner"
```

## 3. download json service account:
```
- mkdir ~/.gcp
- gcloud iam service-accounts keys create ~/.gcp/osServiceAccount.json --iam-account {service-account-name}@{gcp-project}.iam.gserviceaccount.com
```

