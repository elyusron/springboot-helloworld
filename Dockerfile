FROM openjdk:8-alpine

# Required for starting application up.
RUN apk update && apk add /bin/sh

RUN mkdir -p /opt/app

ARG APP_VERSION=1.0.0
ENV APP_VERSION=$APP_VERSION
ENV PROJECT_HOME /opt/app

COPY target/springboot-helloworld-$APP_VERSION.jar $PROJECT_HOME/springboot-helloworld.jar

WORKDIR $PROJECT_HOME


CMD ["java" ,"-jar","./springboot-helloworld.jar"]
