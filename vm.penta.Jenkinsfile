def git_repo = 'https://gitlab.com/pentasys/spring-boot-mongo-docker.git'
def public_route_prefix = 'springboot-vm'
def cluster_url = 'https://ocp.mylabzolution.com:8443'

def git_branch = 'master'
def env = 'demo'
def nexus_base_url = 'http://10.30.30.213:8081'
def nexus_deps_repo = "$nexus_base_url/repository/ist_maven_proxy/"
def nexus_deploy_repo = "$nexus_base_url/repository/maven-releases/"

def ocp_project = 'demo'
def image_pull_secret = 'default-dockercfg-7kc7p'
def oc_command = 'create'

def cpu_limit = '30m'
def memory_limit = '300Mi'
def max_replica_count = 2


def appName
def appFullVersion
def gitCommitId

def remote = [:]
    remote.name = 'rancher'
    remote.host = '10.30.30.57'
    remote.user = 'penta'
    remote.password = 'citicon13th'
    remote.allowAnyHosts = true
    

node (){
    stage('[Code] Checkout') {
        git url: "${git_repo}", branch: "${git_branch}", credentialsId: 'elyusron_gitlab_credential'
    }
    stage('Prepare'){
        withCredentials([[$class: 'UsernamePasswordMultiBinding', 
            credentialsId: 'nexus',
            usernameVariable: 'nexus_username', passwordVariable: 'nexus_password']]) {
                // sh """
                //     echo 'Downloading ci-cd templates...'
                //     curl --fail -u ${nexus_username}:${nexus_password} -o cicd-template.tar.gz ${nexus_base_url}/repository/general-ist/cicd-template-${env}.tar.gz
                //     rm -rf cicd-template
                //     mkdir cicd-template && tar -xzvf ./cicd-template.tar.gz -C "\$(pwd)/cicd-template"
                //     chmod -R 777 "\$(pwd)/cicd-template"
                //     """
                sh """
                    echo 'Downloading ci-cd templates...'
                    curl --fail -u ${nexus_username}:${nexus_password} -o cicd-template.tar.gz ${nexus_base_url}/repository/general-ist/cicd-template-${env}-appx-mylabzolution.tar.gz
                    rm -rf cicd-template
                    tar -xzvf ./cicd-template.tar.gz -C "\$(pwd)/"
                    chmod -R 777 "\$(pwd)/cicd-template"
                    """
                prepareSettingsXml(nexus_deps_repo, nexus_username, nexus_password)
                addDistributionToPom(nexus_deploy_repo)
        }
        
        appName = getFromPom('name')
        if(appName == null || appName.trim() == ""){
          appName = getFromPom('artifactId')
        }
        sh "mvn -s ./cicd-template/maven/settings.xml build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.${BUILD_NUMBER} versions:commit"
        appFullVersion = getFromPom('version')
        gitCommitId = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
        echo "appName: '${appName}', appFullVersion:'${appFullVersion}', gitCommitId:'${gitCommitId}'"
    }
    stage('Jar [Build]') {
        sh 'mvn clean package -D skipTests -s ./cicd-template/maven/settings.xml'
    }
    //stage ('Test'){
    //    sh 'mvn test -s ./cicd-template/maven/settings.xml'
    //}
    //stage('IntegrationTests') {
    //   sh 'mvn failsafe:integration-test -s ./cicd-template/maven/settings.xml'
    //}
    stage('Sonarqube [Test]'){
        sh 'mvn sonar:sonar -Dsonar.host.url=http://10.30.30.215:9000/ -s ./cicd-template/maven/settings.xml' 
    }

    stage ('Nexus Artifact [Release]'){
        sh 'mvn deploy  -DskipTests -s ./cicd-template/maven/settings.xml'
    }

    stage ('[Deploy] Package'){
        sh "cd target && ls"
        // sh "scp /var/lib/jenkins/workspace/springboot-demo-gitlab-cloud-vm-penta/target/${appName}-${appFullVersion}.jar penta@222.165.220.188:/home/penta"   
        sh "scp ${WORKSPACE}/target/${appName}-${appFullVersion}.jar penta@222.165.220.188:/home/penta"      
    }

    // stage ('OpenShift Deployment'){
    //      sh 'ssh penta@10.30.30.57 java -jar /home/penta/${appName}-${appFullVersion}.jar'
    // }
    stage('[Operate] Run Java') {
        sshCommand remote: remote, command: "ls -lrt"
    //   sshCommand remote: remote, command: "killall -9 java"
        sshCommand remote: remote, command: "export BUILD_ID=dontKillMe"
        sshCommand remote: remote, command: "JENKINS_NODE_COOKIE=dontKillMe"
        sshCommand remote: remote, command: "nohup java -jar /home/penta/${appName}-${appFullVersion}.jar > java.log &"
    }
}


def getFromPom(key) {
    sh(returnStdout: true, script: "mvn -s ./cicd-template/maven/settings.xml -q -Dexec.executable=echo -Dexec.args='\${project.${key}}' --non-recursive exec:exec").trim()
}

def addDistributionToPom(nexus_deploy_repo) {
    pom = 'pom.xml'
    distMngtSection = readFile('./cicd-template/maven/pom-distribution-management.xml') 
    distMngtSection = distMngtSection.replaceAll('\\$nexus_deploy_repo', nexus_deploy_repo)

    content = readFile(pom)
    newContent = content.substring(0, content.lastIndexOf('</project>')) + distMngtSection + '</project>'
    writeFile file: pom, text: newContent
}

def prepareSettingsXml(nexus_deps_repo, nexus_username, nexus_password) {
    settingsXML = readFile('./cicd-template/maven/settings.xml') 
    settingsXML = settingsXML.replaceAll('\\$nexus_deps_repo', nexus_deps_repo)
    settingsXML = settingsXML.replaceAll('\\$nexus_username', nexus_username)
    settingsXML = settingsXML.replaceAll('\\$nexus_password', nexus_password)

    writeFile file: './cicd-template/maven/settings.xml', text: settingsXML
}